<?php
/**
 * Plugin Name: BevaOne Customization
 * Plugin URI: https://bevaone.com
 * Description: BevaOne WP Multisite Network all customization code will be writen here. NOTE: This code will automatically activated and can't be controlled in dashboard etc. 
 * Version: 0.1
 * Author: Mohsin Ali
 * Author URI: http://mohsinali.co
 **/
 
if (!defined('ABSPATH')) exit; // Exit if accessed directly


/***** Add Hooks */
function modify_admin_toolbar( $admin_bar ){
    if(! is_super_admin()){
    $admin_bar->add_menu( array(
    'id' => 'view-sites',
    'title' => 'My Sites',
    'href' => site_url( '/wp-admin/my-sites.php' ),
    'meta' => array(
    'title' => __( 'My Sites'),
    'class' => 'my-sites',
    ),
    ) );
	$current_user = wp_get_current_user();
	$user_email = $current_user->user_email;
	$admin_bar->add_menu(array(
        'id' => 'wu-user-email',
        'parent' => 'user-actions',
        'title' => $user_email,
    ));
    $args = array(
    'id'    => 'your-site',
    'title' => 'View Site',
    'href'   => '/', 
    );
    $admin_bar->add_node( $args );
	$c_num_obj = wp_count_comments();
	$comments_number = get_comments_number();
	$c_num = $c_num_obj->total_comments;
    $args = array(
        'id' => 'comments',
        'title' => 'Comments <span class="c_num">'.$c_num.'</span>',
        'parent' => 'top-secondary',
    );
    $admin_bar->add_node($args);

	
    //Move My Account menu item under user profile menu 
    $admin_bar->add_menu(array(
        'id' => 'wu-my-account',
        'parent' => 'my-account',
        'title' => 'My Account<br/><hr>',
    ));  

    
    $args = array(
        'id' => 'invite-members',
        'title' => 'Invite Team Members',
        'href' => '#',
        'parent' => 'my-account',
        'meta' => array(
            'title' => __('Invite Team Members'),
            'class' => 'invite-members',
        ),
    );
    $admin_bar->add_menu($args);

    $args = array(
        'id' => 'premium-plans',
        'title' => 'Premium Plans',
        'href' => '#',
        'parent' => 'my-account',
        'meta' => array(
            'title' => __('Premium Plans'),
            'class' => 'premium-plans',
        ),
    );
    $admin_bar->add_menu($args);

    $args = array(
        'id' => 'refer-earn',
        'title' => 'Refer &amp; Earn',
        'href' => '#',
        'parent' => 'my-account',
        'meta' => array(
            'title' => __('Refer & Earn'),
            'class' => 'refer-earn',
        ),
    );
    $admin_bar->add_menu($args);

    $args = array(
        'id' => 'app-store',
        'title' => 'App Store<br/><hr>',
        'href' => '#',
        'parent' => 'my-account',
        'meta' => array(
            'title' => __('App Store'),
            'class' => 'app-store',
        ),
    );
    $admin_bar->add_menu($args);

    $args = array(
        'id' => 'help-center',
        'title' => 'Help Center',
        'href' => '#',
        'parent' => 'my-account',
        'meta' => array(
            'title' => __('Help Center'),
            'class' => 'help-center',
        ),
    );
    $admin_bar->add_menu($args);

    $args = array(
        'id' => 'news-updates',
        'title' => 'News & Updates',
        'href' => '#',
        'parent' => 'my-account',
        'meta' => array(
            'title' => __('News & Updates'),
            'class' => 'new-updates',
        ),
    );
    $admin_bar->add_menu($args);

    $args = array(
        'id' => 'logout',
        'title' => 'Logout',
        'parent' => 'my-account',
    );
    $admin_bar->add_menu($args);

    // add Teams menu item
    $args = array(
        'id' => 'my-team',
        'title' => '<span id="#open-modal" class="add-team-member">Teams</span>',
        'parent' =>'top-secondary',
        'href' => '#open-modal',
        'meta' => array(
            'title' => __('My Teams'),
            'class' => 'my-teams',
        ),
        'target'   => '_self'
        
    );
    $admin_bar->add_node($args);
}
}
add_action( 'admin_bar_menu', 'modify_admin_toolbar', 90 ); // 10 is left most item and 2000 is the right most item on admin menu bar as priority


add_action( 'admin_enqueue_scripts', 'queue_my_admin_scripts');

function queue_my_admin_scripts() {
    wp_enqueue_script (  'my-spiffy-miodal' ,       // handle
    URL_TO_THE_JS_FILE  ,       // source
                        array('jquery-ui-dialog')); // dependencies
    // A style available in WP               
    wp_enqueue_style (  'wp-jquery-ui-dialog');
}

add_action('init', 'myplugin_load');

function myplugin_load(){
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'jquery-ui-core' );
    wp_enqueue_script( 'jquery-ui-dialog' );
	invitation_based_registrations_register11();
	//add_shortcode('invitation_based_registrations', 'invitation_based_registrations_form');

}

function invitation_based_registrations_register11(){

		if(isset($_GET['invbractiveuser'])){
					
			$inviteid = $_GET['invbractiveuser'];
			if(get_option("invbr_".$inviteid)){
				$issuccess = false;
				$wperror = $username = $first_name = $last_name = $password = "";
				if(isset($_POST['invbr_register'])){
					if(isset($_POST['username'])) $username = sanitize_text_field($_POST['username']);
					if(isset($_POST['pwd'])) $password = sanitize_text_field($_POST['pwd']);
					if(isset($_POST['first_name'])) $first_name = sanitize_text_field($_POST['first_name']);
					if(isset($_POST['last_name'])) $last_name = sanitize_text_field($_POST['last_name']);
					$userdata = array(
						'user_login'  =>  $username,
						'user_email' =>   get_option("invbr_".$inviteid),
						'role' =>  get_option("invbr_role"),
						'first_name' =>   $first_name,
						'last_name'  =>   $last_name,
						'user_pass'   =>  $password,
						'nickname' => $first_name,
						'display_name' => $first_name." ".$last_name
					);
					$user_id = wp_insert_user( $userdata ) ;
					if ( ! is_wp_error( $user_id ) ) {
						$issuccess = true;
					} else {
						$wperror = $user_id->get_error_message();
					}
				}
		
		$invbr_registration_header = get_option("invbr_registration_header") ? get_option("invbr_registration_header") : "Complete your Registration";
		$invbr_registration_username = get_option("invbr_registration_username") ? get_option("invbr_registration_username") : "Username";
		$invbr_registration_email = get_option("invbr_registration_email") ? get_option("invbr_registration_email") : "Email Address";
		$invbr_registration_role = get_option("invbr_role");
		$invbr_registration_password = get_option("invbr_registration_password") ? get_option("invbr_registration_password") : "Password";
		$invbr_registration_first_name = get_option("invbr_registration_first_name") ? get_option("invbr_registration_first_name") : "First Name";
		$invbr_registration_last_name = get_option("invbr_registration_last_name") ? get_option("invbr_registration_last_name") : "Last Name";
		$invbr_registration_submit = get_option("invbr_registration_submit") ? get_option("invbr_registration_submit") : "Register";
		
		echo '<body class="login login-action-login wp-core-ui locale-en-us"><div id="login">
		<link rel="stylesheet" href="'.site_url().'/wp-admin/load-styles.php?c=1&amp;dir=ltr&amp;load%5B%5D=dashicons,buttons,forms,l10n,login&amp" type="text/css" media="all" />
		<form name="loginform" id="loginform" action="" method="post">
		<h2>'.$invbr_registration_header.'</h2><br><hr>';
		
		if(!empty($wperror)){ echo "<span style=color:red>".$wperror."</a><br>"; $wperror = "";}
		else if($issuccess){ 
			$register_redirect_url = get_option("invbr_register_redirect_url");
			if(!empty($register_redirect_url)) {
				echo '<script>window.location.href = "'.$register_redirect_url.'";</script>';
				exit;
			}
			
			echo "<br><span style=color:green>Registration ssssuccessful. You can continue to <a href='".site_url()."/wp-admin'>login here.</a><br></form></body>";exit;
		}
		
		echo '<br><p><label for="user_username">'.$invbr_registration_username.'<br /><input type="text" name="username" id="user_username" class="input" value="'.$username.'" size="20" /></label></p>
		<p><label for="user_email">'.$invbr_registration_email.'<br /><input type="text" name="email" id="user_email" readonly disabled class="input" value="'.get_option("invbr_".$inviteid).'" size="20" /></label></p>
		<p><label for="user_pass">'.$invbr_registration_password.'<br /><input type="password" name="pwd" id="user_pass" class="input" value="'.$password.'" size="20" /></label></p>
		<p><label for="first_name">'.$invbr_registration_first_name.'<br /><input type="text" name="first_name" id="first_name" class="input" value="'.$first_name.'" size="20" /></label></p>
		<p><label for="last_name">'.$invbr_registration_last_name.'<br /><input type="text" name="last_name" id="last_name" class="input" value="'.$last_name.'" size="20" /></label></p>
		<div class="form-field">
		<p><label for="user_role">User Role<br /><input type="text" name="role" id="user_role" readonly disabled class="input" value="'.$invbr_registration_role.'" size="20" /></label></p>
		</div>
		<input type="hidden" name="invbr_register" value="1" />
		<p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="'.$invbr_registration_submit.'" /></p>
	</form><br><br></body>';
			exit;
		} else {
			 wp_die("No Invitation request found for your ID. Please contact your administrator.");
		}
		}
	}
/* function invitation_based_registrations_form(){
	echo '<div>';
		if(isset($_POST['invitation_based_registrations_send_invite']) && isset($_POST['emails'])){
			
			$subject = get_option("invbr_email_subject");
			$emailbody = get_option("invbr_email_body");
			$emailbody = str_replace(PHP_EOL, "<br>", $emailbody);
			$emailsarray = preg_split('/\r\n|[\r\n]/', $_POST['emails']);
			$from_name = get_option("invbr_smtp_from_name");
			$from_email = get_option("invbr_smtp_from_email");
	
			if(!empty($from_name) && !empty($from_email)) {
				$headers = array('From: '.$from_name.' <'.$from_email.'>', 'Content-Type: text/html; charset=UTF-8');
			} else
				$headers = array('Content-Type: text/html; charset=UTF-8');
			 
			if(sizeof($emailsarray)==0) {
				echo "<span style='color:red'>No email address provided.</span><br><br>";
			} else {
				foreach($emailsarray as $email) {
					$email = trim(sanitize_text_field($email));
					if(empty($email))
						continue;
					else if(email_exists( $email )) {
						echo "<div style='max-width:60%;color:black;margin:4px;background:#ffc3c3;padding: 5px 20px;border: 1px solid #ff8c8c;'>User with email <b>".$email." already exists.</b></div>";
						continue;
					}
					$invitationkey = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(32/strlen($x)) )),1,32);
					update_option("invbr_".$invitationkey, $email);
					$emailbodyinner = str_replace("##INVITE_LINK#", site_url().'?invbractiveuser='.$invitationkey ,$emailbody);
					$emailbodyinner = str_replace("##FROM_EMAIL##", $from_email, $emailbodyinner);
					$emailbodyinner = str_replace("##FROM_NAME##", $from_name, $emailbodyinner);
					
					$subject = str_replace("##FROM_EMAIL##", $from_email, $subject);
					$subject = str_replace("##FROM_NAME##", $from_name, $subject);
					
					wp_mail( $email, $subject, $emailbodyinner, $headers );
					echo "<div style='max-width:60%;color:black;margin:4px;background: #cdfbaa;padding: 5px 20px;border: 1px solid #b8d6a1;'>Invitation has been sent to <b>".$email."</b></div>";
					update_option("invbr_".$invitationkey, $email);
				}
				echo "<br>";
			}
	
		}
		
	invitation_based_registrations_send_invite();
	
	echo '</div>';
	
} */
	
	
/* function invitation_based_registrations_send_invite(){
	
	echo '<div style="margin:4px;padding:10px 40px;max-width:850px">
	<a href="https://wordpress.org/support/plugin/invitation-based-registrations" target="_blank"><button style="float:right;background: #58c551;color: #fff;padding: 5px;font-size: 14px;">Request new feature or give feedback</button></a>
	<h3>Invite Users to Register</h3>';
	$subject = 'Invitation to create account | '.get_bloginfo();
	if(get_option("invbr_email_subject"))
	$subject = get_option("invbr_email_subject");
	$emailbody = 'Hey there,&#10;&#10;You have been invited to create account with '.get_bloginfo().'. Click <a href="##INVITE_LINK#">this link</a> to complete registration.&#10;&#10;Regards,&#10;'.get_bloginfo();
	if(get_option("invbr_email_body")) {
		$emailbody = get_option("invbr_email_body");
	}
	$emailbody = str_replace(PHP_EOL, "<br>", $emailbody);
	
	echo '<form  method="POST" action="">
		<input type="hidden" name="invitation_based_registrations_send_invite" value="1"/>
		<a href="'.add_query_arg( array('page' => 'invite_users_template'), $_SERVER['REQUEST_URI'] ).'">Customize Email Templates</a><br><br>
		<font style="color:#f00">* </font>Email ID\'s to invite </b> (each on new line) : <br>
		<textarea rows="8" cols="120" required="true"  name="emails"  placeholder="List of email addresses, each on new line" value="" style="min-width:250px"></textarea><br><br>
		<input type="submit" class="button button-primary button-large" value="Send Invitation">
		
		</form>
	</div>';
	
} */
function wpse_form_in_admin_bar() {
    global $wp_admin_bar;

    if ( isset($_POST['submit'] ) ) {    
		$first_name = $_POST['username'];
        $last_name = $_POST['password'];
        $username = $_POST['email'];
        $email = $_POST['email'];
        $role = $_POST['role'];
        $password =  $_POST['password'];
		?><pre><?php print_r($role); ?></pre><?php

       /*  $userdata = array(
         'first_name' =>   $first_name,
         'last_name' =>   $last_name,
         'user_login' =>   $username,
         'user_email' =>   $email,
         'user_pass' =>   $password,

        ); */
      //  $user = wp_insert_user( $userdata );
	  

		$subject = get_option("invbr_email_subject");
		$emailbody = get_option("invbr_email_body");
		$emailbody = str_replace(PHP_EOL, "<br>", $emailbody);
		$emailsarray = preg_split('/\r\n|[\r\n]/', $_POST['emails']);
		$from_name = get_option("invbr_smtp_from_name");
		$from_email = get_option("invbr_smtp_from_email");
		//$subject = 'Invitation to create account';
/* 		$emailbody = 'Hey there,
		You have been invited to create account with Jacket Valley. Click <a href="##INVITE_LINK##">this link</a> to complete registration.
		Regards,
		Jacket Valley';
		$emailbody = str_replace(PHP_EOL, "<br>", $emailbody);
		$emailsarray = preg_split('/\r\n|[\r\n]/', $_POST['emails']);
		$from_name = 'Hassan Iqbal';
		$from_email = 'hassaniqbal.dev@gmail.com'; */

		if(!empty($from_name) && !empty($from_email)) {
			$headers = array('From: '.$from_name.' <'.$from_email.'>', 'Content-Type: text/html; charset=UTF-8');
		} else{
			$headers = array('Content-Type: text/html; charset=UTF-8');
		}
		$email = trim(sanitize_text_field($email));
		if(email_exists( $email )) {
			echo "<div style='max-width:60%;color:black;margin:4px;background:#ffc3c3;padding: 5px 20px;border: 1px solid #ff8c8c;'>User with email <b>".$email." already exists.</b></div>";
		}
		$invitationkey = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(32/strlen($x)) )),1,32);
		update_option("invbr_role", $role);
		update_option("invbr_".$invitationkey, $email);
		$emailbodyinner = str_replace("##INVITE_LINK#", site_url().'?invbractiveuser='.$invitationkey ,$emailbody);
		$emailbodyinner = str_replace("##FROM_EMAIL##", $from_email, $emailbodyinner);
		$emailbodyinner = str_replace("##FROM_NAME##", $from_name, $emailbodyinner);
		
		$subject = str_replace("##FROM_EMAIL##", $from_email, $subject);
		$subject = str_replace("##FROM_NAME##", $from_name, $subject);
		/* ?><pre><?php print_r($email); ?></pre><?php */
		wp_mail( $email, $subject, $emailbodyinner, $headers );
		echo "<div style='max-width:60%;color:black;margin:4px;background: #cdfbaa;padding: 5px 20px;border: 1px solid #b8d6a1;'>Invitation has been sent to <b>".$email."</b></div>";
		update_option("invbr_".$invitationkey, $email);
		echo "<br>";	  
		
}
	$users = get_users( array( 'fields' => array( 'ID' ) ) );
foreach($users as $user_id) {
	
   // $user_ids .= '<p class="info-text">'..'</p>';
	
	$user_info = get_userdata($user_id->ID);
	
	 $users_data .= '<li>
			<div class="member">
				<div class="member-avatar">
					<img src="https://www.gravatar.com/avatar/bcf7bd535c3eb6b962b5e2860f6701ec?s=50" alt="" style="border-radius: 50%">
				</div>
				<div class="member-content">
					<span class="member-name">' . $user_info->user_login .'</span>
					<div class="member-desc">
					<span class="member-email">'.$user_info->user_email.'</span>
					<span class="member-priority">- '.$user_info->roles[0].'</span>
					<span class="member-priority">- Approved</span>
					</div>
				</div>
			</div>
		</li>';
						
}	


    $wp_admin_bar->add_menu( array(
        'id' => 'hidden_users_popup',
        'parent' => 'top-secondary',
        'title' => '<div class="modal fade team-member-modal" tabindex="-1" role="dialog" id="teamMemberModal" style="display: none; padding-left: 15px;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="info-modal" id="modal-response">
                <i class="info-icon"></i>
                <p class="info-text"></p>
            </div>
            <a href="#" class="btn-close" data-dismiss="modal" hidefocus="true" style="outline: none;"></a>
            <div class="modal-header">
                <h5 class="modal-title">Team Members</h5>
                <h6 class="modal-title-desc">
                    There are 1 people collaborating in this account
                </h6>
            </div>
            <div class="modal-body">
                <ul class="team-member-list">'. $users_data.'</ul>
                                    <div class="invite-team-member-wrap">
                        <h5 class="invite-title">INVITE TEAM MEMBERS</h5>
                        <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
                            
							<div class="row">
								<div class="col-sm-5 form-field">
									<input type="email" name="email" placeholder="Enter an email address" hidefocus="true" style="outline: none;">
								</div>
								<div class="col-sm-3 form-field">
									<select name="role" id="role" required>
										<option value="">Select Role</option>
										<option value="contributor">Contributor</option>
										<option value="author">Author</option>
										<option value="editor">Editor</option>
										<option value="account_manager">Account Manager</option>
									</select>
								</div>
								<div class="col-sm-2 form-field">
									<input hidefocus="true" style="outline: none;" id="btn-invite-team-member" class="btn btn-2 btn-choose" type="submit" name="submit" value="Send"/>
								</div>
                            </div>
                        </form>
                    </div>
				</div>
        </div>
    </div>
</div>'
    ) );
}
add_action( 'admin_bar_menu', 'wpse_form_in_admin_bar' );


add_filter( 'admin_bar_menu', 'replace_wordpress_howdy', 25 );
function replace_wordpress_howdy( $wp_admin_bar ) {
$my_account = $wp_admin_bar->get_node('my-account');
$newtext = str_replace( 'Howdy,', '', $my_account->title );
$wp_admin_bar->add_node( array(
'id' => 'my-account',
'title' => $newtext,
) );
$wp_admin_bar->add_menu( array(
        'id'    => 'menu-id',
        'parent' => null,
        'group'  => null,
        'title' => 'Menu Title', //you can use img tag with image link. it will show the image icon Instead of the title.
        'href'  => admin_url('admin.php?page=custom-page'),
        'meta' => [
            'title' => __( 'Menu Title', 'textdomain' ), //This title will show on hover
        ]
) );
}

function bevaone_admin_menu()
{
    if(! is_super_admin()){
    global $menu;
    $url = 'https://bevaone.com';
    $menu[0] = array( __('BevaONE Beta'), 'read', $url, 'bevaone-logo', 'bevaone-logo');
    $menu[1000000] = array( __('Upgrade'), 'read', $url, 'upgrade-mebership', 'upgrade-membership');
    }
}
add_action('admin_menu', 'bevaone_admin_menu');




/***** Remove Hooks */

function bevaone_remove_admin_bar_menu($wp_admin_bar){
    if(! is_super_admin()){
    $wp_admin_bar->remove_menu('wp-logo'); // Remove WordPress Logo
    $wp_admin_bar->remove_menu('my-sites'); // Remove My Sites 
    $wp_admin_bar->remove_menu('site-name'); // Remove Website default name
    //$wp_admin_bar->remove_menu('comments'); // Remove comments
    $wp_admin_bar->remove_menu('new-content'); // Remove Add New Contents 
    $wp_admin_bar->remove_menu('customize');
    $wp_admin_bar->remove_menu('edit-profile');
    //$wp_admin_bar->remove_menu('logout');

    }   
}
add_action('admin_bar_menu','bevaone_remove_admin_bar_menu', 99);

 /* Remove Admin Dashboard Menu Links
 function remove_menu_items() {
    if(! is_super_admin()){
    global $menu;
    $restricted = array(__('Links'), __('Comments'), __('Media'),
    __('Plugins'), __('Tools'), __('Users'));
    end ($menu);
    while (prev($menu)){
      $value = explode(' ',$menu[key($menu)][0]);
      if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){
        unset($menu[key($menu)]);}
      }
    }
}  
add_action('admin_menu', 'remove_menu_items');*/


/***** [ Load Admin Styles and Scripts ] *****/
 function ma_custom_scripts(){
     if(! is_super_admin()){
        wp_enqueue_style('bevaone-admin-styles', site_url().'/wp-content/mu-plugins/styles.css', false, '1.0');
     }
     
 }
add_action('admin_enqueue_scripts','ma_custom_scripts');

/***** [ Load Admin Scripts ] *****/
 function ma_custom_scripts1(){
     if(! is_super_admin()){
        wp_enqueue_script('bevaone-admin-script1', site_url().'/wp-content/mu-plugins/script.js', false, '1.0');
     }     
 }
add_action('admin_enqueue_scripts','ma_custom_scripts1');




//wp_register_script( 'some-js', get_template_directory_uri().'/js/some.js', array('jquery-core'), false, true );
    //wp_enqueue_script( 'some-js' );
